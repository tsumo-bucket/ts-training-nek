<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageContentItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_content_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_id');
            $table->string('title');
            $table->text('description');
            $table->integer('image');
            $table->integer('bg_image');
            $table->enum('type', ['default', 'image', 'input', 'file upload'])->default('default');
            $table->enum('published', ['draft', 'published'])->default('draft');
            $table->integer('order');
            $table->timestamps();
            $table->softDeletes();
            $table->index(['content_id']);
            $table->string('name')->nullable();
			$table->text('slug')->nullable();
			$table->tinyInteger('enabled')->default(1);
			$table->tinyInteger('editable')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_content_items');
    }
}
